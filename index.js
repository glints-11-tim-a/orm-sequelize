const express = require("express"); // Import Express
const app = express(); // Create app from express

// Import Routes
const transaksiRoutes = require("./routes/transaksiRoute.js");
const barangRoutes = require("./routes/barangRoute.js");
const pelangganRoutes = require("./routes/pelangganRoute.js");
const pemasokRoutes = require("./routes/pemasokRoute.js");

// implement body reader
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// Import table relationship
require("./utils/assocs");

app.use(express.static('public'));

// Implement Routes
app.use("/transaksi", transaksiRoutes);
app.use("/barang", barangRoutes);
app.use("/pelanggan", pelangganRoutes);
app.use("/pemasok", pemasokRoutes);

let PORT = 3000;
app.listen(PORT, ()=> console.log(`listen to port ${PORT}`) ); // make application have port 3000