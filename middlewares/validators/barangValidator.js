const pemasok = require("../../models").pemasok;
const barang = require("../../models").Barang; // Import barang model
const validator = require("validator");

class BarangValidator {
  async valCreate(req, res, next) {
    try {
      // Create errors variable
      let errors = [];

      let findPemasok = await pemasok.findOne({
        where: { id: req.body.id_pemasok },
      });

      if (!findPemasok) {
        errors.push("Data pemasok belum terdaftar");
      }
      // Set form validation rule
      if (!validator.isAlpha(validator.blacklist(req.body.nama, " "))) {
        errors.push("Nama must only use alphabets");
      }
      if (!validator.isNumeric(req.body.harga)) {
        errors.push("Harga must be a number");
      }
      if (!validator.isNumeric(req.body.id_pemasok)) {
        errors.push("ID pemasok must be a number");
      }

      // If errors length > 0, it will make errors message
      if (errors.length > 0) {
        // Because bad request
        return res.status(400).json({
          message: errors.join(", "),
        });
      }
      next();
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        error: err,
      });
    }
  }

  async valUpdate(req, res, next) {
    try {
      // Find barang data
      let findData = await barang.findOne({
        where: { id: req.params.id },
      });

      let findPemasok = await pemasok.findOne({
        where: { id: req.body.id_pemasok },
      });

      // Create errors variable
      let errors = [];
      // If barang not found
      if (!findData) {
        errors.push("Data barang belum terdaftar");
      }

      if (!findPemasok) {
        errors.push("Data pemasok belum terdaftar");
      }

      //Set form validation rule
      if (!validator.isAlpha(validator.blacklist(req.body.nama, " "))) {
        errors.push("Nama must only use alphabets");
      }
      if (!validator.isNumeric(req.body.harga)) {
        errors.push("Harga must be a number");
      }
      if (!validator.isNumeric(req.body.id_pemasok)) {
        errors.push("ID pemasok must be a number");
      }

      // If errors length > 0, it will make errors message
      if (errors.length > 0) {
        // Because bad request
        return res.status(400).json({
          message: errors.join(", "),
        });
      }
      next();
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        error: err,
      });
    }
  }
}

module.exports = new BarangValidator();
