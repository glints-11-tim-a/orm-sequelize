const transaksi = require("../../models").transaksi;
const pemasok = require("../../models").pemasok;
const barang = require("../../models").Barang;
const pelanggan = require("../../models").Pelanggan;
const {
  check,
  validationResult,
  matchedData,
  sanitize,
} = require("express-validator"); //form validation & sanitize form params

module.exports.create = async (req, res, next) => {
  try {
    // Create errors variable
    let errors = [];
    // Get the price
    let price;
    //Set form validation rule

    //validasi id_barang
    let cekIdBarang = check("id_barang").isNumeric();
    if (cekIdBarang) {
      let getBarang = await barang.findOne({where:{id:req.body.id_barang}});
      price = getBarang.harga;
      if (price===null) {
        errors.push("ID barang tidak ada!");
      }
    } else errors.push("ID barang harus angka!");
    //validasi id Pelanggan
    let cekIdPelanggan = check("id_pelanggan").isNumeric();
    if (cekIdPelanggan) {
      let cek = await pelanggan.findOne({where:{id:req.body.id_pelanggan}});
      if (!cek.id) {
         errors.push("ID pelanggan tidak ada!");
      }
    } else errors.push("ID pelanggan harus angka!");
    //cek inputan jumlah
    let cekJumlah = check("jumlah").isNumeric();
    if (!cekJumlah) errors.push("Jumlah harus angka");

    // If errors length > 0, it will make errors message
    if (errors.length > 0) {
      // Because bad request
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    // req.body.total add value
    req.body.total = eval(price * req.body.jumlah);
    next();
  } catch (e) {
    console.log(e);
    return res.status(500).json({ error: e });
  }
}; // end export create

module.exports.update = async (req, res, next) => {
  try {
    // Create errors variable
    let errors = [];
    //Set form validation rule
    let price;
      let cekIdTransaksi = check("id").isNumeric();
      if (cekIdTransaksi) {
        let getTransaksi = await transaksi.findOne({where:{id:req.params.id}});
        if (!getTransaksi.id) {
          errors.push("ID transaksi tidak ada!");
        }
      } else errors.push("ID transaksi harus angka!");

      //validasi id_barang
      let cekIdBarang = check("id_barang").isNumeric();
      if (cekIdBarang) {
        let getBarang = await barang.findOne({where:{id:req.body.id_barang}});
        price = getBarang.harga;
        if (price===null) {
          errors.push("ID barang tidak ada!");
        }
      } else errors.push("ID barang harus angka!");

      //validasi id Pelanggan
      let cekIdPelanggan = check("id_pelanggan").isNumeric();
      if (cekIdPelanggan) {
        let cek = await pelanggan.findOne({where:{id:req.body.id_pelanggan}});
        if (!cek.id) {
           errors.push("ID pelanggan tidak ada!");
        }
      } else errors.push("ID pelanggan harus angka!");

    //cek inputan jumlah
    let cekJumlah = check("jumlah").isNumeric();
    //bila sudah cek semua, print errornya klo ada
    if (!cekIdTransaksi) errors.push("ID Transaksi tidak ada!");
    if (!cekJumlah) errors.push("Jumlah harus angka");

    // If errors length > 0, it will make errors message
    if (errors.length > 0) {
      // Because bad request
      return res.status(400).json({
        message: errors.join(", "),
      });
    }
    // req.body.total add value
    req.body.total = eval(price * req.body.jumlah);
    console.log(req.body.total );
    next();
  } catch (e) {
    console.log(e);
    return res.status(500).json({ error: e });
  }
};
