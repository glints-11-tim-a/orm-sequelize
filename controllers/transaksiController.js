const transaksi = require("../models").transaksi;
const pemasok = require("../models").pemasok;
const barang = require("../models").Barang;
const pelanggan = require("../models").Pelanggan;

class TransaksiController {
  // Get All data from transaksi
  async getAll(req, res) {
    try {
      let dataTrans = await transaksi.findAll({
        include: [
          {
            model: barang,
            attributes: ["nama"], // just this attrubute from Barang that showed
            include: [
              {
                model: pemasok,
                attributes: ["nama"],
              },
            ],
          },
          {
            model: pelanggan,
            attributes: ["nama"], // just this attrubute from Pelanggan that showed
          },
        ],
      });
      if (!dataTrans) {
        return res.status(500).json({ message: "error", error: dataTrans });
      } else {
        let arrData = [];
        dataTrans.forEach((transaksi) => {
          let data = {
            id: transaksi.id,
            jumlah: transaksi.jumlah,
            total: transaksi.total,
            createdAt: transaksi.createdAt,
            updatedAt: transaksi.updatedAt,
            fakturImg: transaksi.fakturImg,
            barang: transaksi.Barang.nama,
            pemasok: transaksi.Barang.pemasok.nama,
            pelanggan: transaksi.Pelanggan.nama,
          };
          arrData.push(data);
        });

        return res.status(200).json({ message: "success", data: arrData }); // Send response JSON and get all of Transaksi
      }
    } catch (e) {
      console.log(e);
      res.status(500).json({
        message: "error catch",
        error: e,
      });
    }
  } // end get ALL

  // Get All data from transaksi
  async getOne(req, res) {
    transaksi
      .findOne({
        // find all data of Transaksi table
        where: { id: `${req.params.id}` },
        //attributes: ["id", "jumlah", "total", ["createdAt", "waktu"]], // attributes ga usah di pakai klo mau select ALL
        include: [
          {
            model: barang,
            attributes: ["nama"], // just this attrubute from Barang that showed
            include: [
              {
                model: pemasok,
                attributes: ["nama"],
              },
            ],
          },
          {
            model: pelanggan,
            attributes: ["nama"], // just this attrubute from Pelanggan that showed
          },
        ],
      })
      .then((transaksi) => {
        console.log(transaksi.id);
        let data = {
          id: transaksi.id,
          jumlah: transaksi.jumlah,
          total: transaksi.total,
          createdAt: transaksi.createdAt,
          updatedAt: transaksi.updatedAt,
          fakturImg: transaksi.fakturImg,
          barang: transaksi.Barang.nama,
          pemasok: transaksi.Barang.pemasok.nama,
          pelanggan: transaksi.Pelanggan.nama,
        };
        res.status(200).json({ message: "success", data: data }); // Send response JSON and get all of Transaksi
      })
      .catch((err) => {
        console.log(err);
        res.json(err);
      });
  } // end get Ones

  // create data from transaksi
  async create(req, res) {
    try {
      //hitung total harga
      let insertAttr = {
        id_barang: req.body.id_barang,
        id_pelanggan: req.body.id_pelanggan,
        jumlah: req.body.jumlah,
        fakturImg: req.body.image && req.body.image,
        total: req.body.total,
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      let createTrans = await transaksi.create(insertAttr);

      // cek apakah data berhasil di input
      if (!createTrans) {
        return res.status(500).json({
          message: "transaksi gagal di input",
          error: createTrans,
        });
      } else {

        return res.status(200).json({
          message: "success",
          data: createTrans,
        });
      }
    } catch (e) {
      console.log(e);
      res.status(500).json(e);
    }
  } // end insert

  // update data from transaksi
  async update(req, res) {
    try {
      let updateAttr = {
        id_barang: req.body.id_barang,
        id_pelanggan: req.body.id_pelanggan,
        jumlah: req.body.jumlah,
        total: req.body.total,
        fakturImg: req.body.image,
        updatedAt: new Date(),
      };
      let updateTrans = await transaksi.update(updateAttr, {
        where: { id: req.params.id },
      });

      if (updateTrans[0] === 0) {
        return res.status(500).json({ message: "transaksi belum terdaftar" });
      }
      //menampilkan data yang diupdate
      let findTransOne = await transaksi.findOne({
        where: { id: req.params.id },
      });

      return res.status(200).json({
        message: "success",
        data: findTransOne,
      });
    } catch (e) {
      console.log(e);
      res.status(500).json(e);
    }
  } // end Update

  async delete(req, res) {
    let deleteData = await transaksi.destroy({
      // Delete data from Transaksi table
      where: {
        id: req.params.id, // Where id of Transaksi table is equal to req.params.id
      },
    });
    if (deleteData) {
      return res.status(200).json({
        status: "success",
        message: "transaksi deleted",
        data: null,
      });
    } else {
      return res.status(500).json({
        status: "success",
        message: "delete fail",
        data: null,
      });
    }
  }
}

module.exports = new TransaksiController();
