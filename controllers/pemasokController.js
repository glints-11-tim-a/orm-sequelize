const { pemasok } = require("../models");
const {
  check,
  validationResult,
  matchedData,
  sanitize,
} = require("express-validator"); //form validation & sanitize form params
// //const { where } = require("sequelize/types");

class PemasokController {
  // Get All data from pemasok
  async getAll(req, res) {
    try {
      let data = await pemasok.findAll({
        attributes: [
          "nama",
          "alamat",
          "email",
          "no_hp",
          "image",
          ["createdAt", "tanggal_dibuat"],
        ],
      });
      if (data.length === 0) {
        return res.status(404).json({
          message: "Data Not Found",
        });
      }
      return res.status(200).json({
        message: "Success",
        data: data,
      }); 
    } catch (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
  } // end get ALL

  // Get One data from pemasok
  async getOne(req, res) {
    try {
      let data = await pemasok.findOne({
        where: { id: `${req.params.id}` },
        attributes: [
          "id",
          "nama",
          "alamat",
          "email",
          "no_hp",
          "image",
          ["createdAt", "tanggal_dibuat"],
        ],
      });
      if (!data) {
        return res.status(404).json({
          message: "Data Not Found",
        });
      }
      return res.status(200).json({
        message: "Success",
        data: data,
      }); 
    } catch (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
  } // end get ALL

  // create data for pemasok
  async create(req, res) {
    try {
      let createdData = await pemasok.create({
        nama: req.body.nama,
        alamat: req.body.alamat,
        email: req.body.email,
        no_hp: req.body.no_hp,
        image: req.body.image && req.body.image, //masih bingung
      });
      let data = await pemasok.findOne({
        where: { id: createdData.id },
        attributes: [
          "id",
          "nama",
          "alamat",
          "email",
          "no_hp",
          "image",
          ["createdAt", "tanggal_dibuat"],
        ],
      });
      console.log(req.body.image);
      return res.status(201).json({
        message: "Success",
        data: data,
      });
    } catch (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    } // end insert
  }

  // update data from transaksi
  async update(req, res) {
    try {
      let update = {
        nama: req.body.nama,
        alamat: req.body.alamat,
        email: req.body.email,
        no_hp: req.body.no_hp,
        image: req.body.image && req.body.image,
      };
      //Find the updated transaksi
      let updateData = await pemasok.update(update, {
        where: {
          id: `${req.params.id}`
        },
      });

      if (!updateData) {
        return res.status(404).json({
          message: `Data Not Found`,
        });
      }

      let data = await pemasok.findOne({
        where: {
          id: `${req.params.id}` 
        },
        attributes: [
          "id",
          "nama",
          "alamat",
          "email",
          "no_hp",
          "image",
          ["createdAt", "tanggal_dibuat"],
        ],
      });

      return res.status(200).json({
        message: "Success",
        data: data,
      });
    } catch (err) {
      // If error
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
  }

  //delete one pemasok from database
  async delete(req, res) {
    try {
      // Delete data
      let data = await pemasok.destroy({ 
        where: { 
          id: `${req.params.id}` 
        } 
      });
      // If data deleted is null
      if (!data) {
        return res.status(404).json({
          message: "Data Not Found",
        });
      }
      // If success
      return res.status(200).json({
        message: "Success",
      });
    } catch (err) {
      // If error
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
  }
}

module.exports = new PemasokController();
