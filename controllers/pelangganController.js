const pelanggan = require("../models").Pelanggan;

class PelangganController {
  // Get all Pelanggan
  async getAll(req, res) {
    try {
      let allData = await pelanggan.findAll({
        attributes: ["id", "nama", "alamat", "noTelp", ["createdAt", "waktu"]], // just these attributes that showed
      });

      if (allData.length === 0) {
        return res.status(404).json({
          message: "Data tidak ditemukan",
        });
      }
      return res.status(200).json({
        message: "Data berhasil ditampilkan",
        allData,
      });
    } catch (e) {
      console.error(e);
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  // Get One Pelanggan
  async getOne(req, res) {
    try {
      let dataOne = await pelanggan.findOne({
        where: { id: `${req.params.id}` },
        attributes: ["id", "nama", "alamat", "noTelp", ["createdAt", "waktu"]], // just these attributes that showed
      });

      if (!dataOne) {
        return res.status(404).json({
          message: "Id pelanggan tidak ditemukan",
        });
      }

      return res.status(200).json({
        message: "Berhasil",
        data: dataOne,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async create(req, res) {
    try {
      let createData = await pelanggan.create({
        nama: req.body.nama,
        alamat: req.body.alamat,
        noTelp: req.body.noTelp,
        image: req.body.image,
      });

      let dataSuccess = await pelanggan.findOne({
        where: { id: createData.id },
      });
      if (!dataSuccess) {
        return res.status(500).json({
          message: "Data diri pelanggan gagal di input",
          error: dataSuccess,
        });
      } else {
        return res.status(201).json({
          message: "Penambahan data pelanggan berhasil",
          data: dataSuccess,
        });
      }
    } catch (e) {
      console.error(e);
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async update(req, res) {
    try {
      console.log(req.params.id);
      let updateData = await pelanggan.update(
        {
          nama: req.body.nama,
          alamat: req.body.alamat,
          noTelp: req.body.noTelp,
          image: req.body.image,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      );

      let dataSuccess = await pelanggan.findOne({
        where: { id: req.params.id },
      });
      if (!dataSuccess) {
        return res.status(500).json({
          message: "Data diri pelanggan gagal di perbarui",
          error: dataSuccess,
        });
      } else {
        return res.status(201).json({
          message: "Pembaruan data pelanggan berhasil",
          data: dataSuccess,
        });
      }
    } catch (e) {
      console.log(e);
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async deletePelanggan(req, res) {
    let deletePelanggan = await pelanggan.destroy({
      where: { id: req.params.id }, // Where id of Pelanggan table is equal to req.params.id
    });
    if (deletePelanggan) {
      return res.status(200).json({
        status: "success",
        message: "id dan nama pelanggan telah terhapus",
      });
    } else {
      return res.status(500).json({
        status: "gagal",
        message: "penghapusan gagal",
        data: null,
      });
    }
  }
}

module.exports = new PelangganController();
