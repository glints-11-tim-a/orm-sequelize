const pemasok = require("../models").pemasok;
const barang = require("../models").Barang;// Import all models

class BarangController {
  // Get All data from Barang
  async getAll(req, res) {
    try {
      let data = await barang.findAll({
        // find all data in Barang Table
        attributes: [
          "id",
          "nama",
          "harga",
          "image",
          ["createdAt", "waktu"],
        ],
        include: [
          {
            model: pemasok,
            attributes: ["id"],
          },
        ],
      });
      if (data.length === 0) {
        return res.status(404).json({
          message: "Data Not Found",
        });
      }
      return res.status(200).json({
        message: "Success",
        data: data,
      });
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
  } // end getAll

  // Get One data from barang
  async getOne(req, res) {
    try {
      let data = await barang.findOne({
        where: { id: `${req.params.id}` },
        attributes: [
          "id",
          "nama",
          "harga",
          "image",
          ["createdAt", "waktu"],
        ],
        include: [
          {
            model: pemasok,
            attributes: ["id"],
          },
        ],
      });
      if (!data) {
        return res.status(404).json({
          message: "Data Not Found",
        });
      }
      return res.status(200).json({
        message: "Success",
        data: data,
      });
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
  } // end get One

  // create data for barang
  async create(req, res) {
    try {
      let createdData = await barang.create({
        nama: req.body.nama,
        harga: req.body.harga,
        id_pemasok: req.body.id_pemasok,
        image: req.body.image && req.body.image, // masih bingung
      });
      let data = await barang.findOne({
        where: { id: createdData.id },
        attributes: [
          "id",
          "nama",
          "harga",
          "image",
          ["createdAt", "waktu"],
        ],
        include: [
          {
            model: pemasok,
            attributes: ["id"],
          },
        ],
      });
      return res.status(201).json({
        message: "Success",
        data: data,
      });
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    } //
  } // end of Create

  // update data in barang
  async update(req, res) {
    try {
      let update = {
        nama: req.body.nama,
        harga: req.body.harga,
        id_pemasok: req.body.id_pemasok,
        image: req.body.image && req.body.image, // masih bingung
      };
      // Find the updated transaksi
      let updateData = await barang.update(update, {
        where: {
          id: `${req.params.id}`,
        },
      });

      if (!updateData) {
        return res.status(404).json({
          message: `Data Not Found`,
        });
      }

      let data = await barang.findOne({
        where: {
          id: `${req.params.id}`,
        },
        attributes: [
          "id",
          "nama",
          "harga",
          "image",
          ["createdAt", "waktu"],
        ],
        include: [
          {
            model: pemasok,
            attributes: ["id"],
          },
        ],
      });

      return res.status(200).json({
        message: "Success",
        data: data,
      });
    } catch (err) {
      // If error
      console.log(err);
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
  }

  // delete one barang from database
  async delete(req, res) {
    try {
      // Delete data
      let data = await barang.destroy({
        where: {
          id: `${req.params.id}`,
        },
      });
      // If data deleted is null
      if (!data) {
        return res.status(404).json({
          message: "Data Not Found",
        });
      }
      // If success
      return res.status(200).json({
        message: "Success",
      });
    } catch (err) {
      // If error
      console.log(err);
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
  }
}

module.exports = new BarangController();
