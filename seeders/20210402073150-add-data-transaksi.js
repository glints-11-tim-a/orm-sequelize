'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('transaksi', [{
      id_barang: 1,
      id_pelanggan: 1,
      jumlah: 3,
      total: 909090,
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      id_barang: 2,
      id_pelanggan: 2,
      jumlah: 7,
      total: 21000,
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      id_barang: 2,
      id_pelanggan: 1,
      jumlah: 9,
      total: 90000,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
  ])
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('transaksi', null, {})
  }
};
