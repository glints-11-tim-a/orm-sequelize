"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("pemasok", [
      {
        nama: "Unilever",
        alamat: "Jl Tanah Mas Raya Bl C/8, Cibubur",
        email: "email@unilever.com",
        no_hp: 6286534821010,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Indofood",
        alamat: "Jl Lamau I 22, Palembang",
        email: "email@indofood.com",
        no_hp: 6285738389456,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Mayora",
        alamat: "Jl Pembangunan II/12, Ciamis",
        email: "email@mayora.com",
        no_hp: 6281455559090,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Garudafood",
        alamat: "Jl RS Fatmawati 39, Cianjur",
        email: "email@garudafood.com",
        no_hp: 6215678789999,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Nestle",
        alamat: "Jl Jend Sudirman Kav 29-31 Ged World Trade Center Lt Dasar, Sidoarjo",
        email: "email@nestle.com",
        no_hp: 6281722333444,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Coca-Cola",
        alamat: "Jl Semar Kios 8-9 K, Malang",
        email: "email@cocacola.com",
        no_hp: 6281100007654,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("pemasok", null, {});
  },
};
