'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Pelanggan', [{
      nama: "Jhorgi",
      alamat: "Jl Kuy no 1,Sabang",
      noTelp: "0811223344",
      createdAt: new Date(),
      updatedAt: new Date(),
      deletedAt: new Date()
    }, {
      nama: "Karina",
      alamat: "Jl Sesama no 2,Merauke",  
      noTelp: "0822334455",
      createdAt: new Date(),
      updatedAt: new Date(),
      deletedAt: new Date()
    }, {
      nama: "Amril",
      alamat: "Jl Bentar no 3,Rote",
      noTelp: "0833445566",
      createdAt: new Date(),
      updatedAt: new Date(),
      deletedAt: new Date()
    }, {
      nama: "Risa",
      alamat: "Jl Raya no 4,Miangas",
      noTelp: "0844556677",
      createdAt: new Date(),
      updatedAt: new Date(),
      deletedAt: new Date()
    }])
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Pelanggan', null, {})
  }
};
