const express = require("express"); // Import express
const router = express.Router(); // Make router from app

//import middleware
const { imageUpload
 } = require("../middlewares/uploads/imageUpload");
const pemasokController = require("../controllers/pemasokController.js");
const pemasokValidator = require("../middlewares/validators/pemasokValidator.js");

//router path
router.get("/", pemasokController.getAll);
router.get("/:id", pemasokController.getOne);
router.post(
  "/",
  imageUpload,
  pemasokValidator.validator,
  pemasokController.create
);
router.put(
  "/:id",
  imageUpload,
  pemasokValidator.validator,
  pemasokController.update
);
router.delete("/:id", pemasokController.delete);

module.exports = router; // Export router
