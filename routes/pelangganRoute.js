const express = require("express"); // Import express
const router = express.Router(); // Make a router
const pelangganController = require('../controllers/pelangganController.js') // Import TransaksiController
const pelangganValidator = require('../middlewares/validators/pelangganValidators.js') // Import validator to validate every request from user

//import image
const { imageUpload } = require("../middlewares/uploads/imageUpload");

router.get('/', pelangganController.getAll)
router.get('/:id', pelangganController.getOne)
router.post('/create', imageUpload, pelangganValidator.create, pelangganController.create)
router.put('/update/:id',imageUpload, pelangganValidator.update, pelangganController.update)
router.delete('/:id', pelangganController.deletePelanggan) 


module.exports = router; // Export router
