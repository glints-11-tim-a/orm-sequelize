# **Assignment Basic CRUD with ORM Sequelize**
**Tim A:**
1. Amril (@amrilazhar)
2. Risa (@azkafarisa)
3. Jhorgi (@jhorgihisamawa)
4. Karina (@karinasarahk)

**Reviewer :** Fahmi Alfareza (@fahmialfareza)

# **Usage**
In this section we will explain how to use/run this project. It will divide into three section :
1. **Installation and Configuration**
2. **Basic Usage**
    1. CRUD Table Barang
    2. CRUD Table Transaksi
    3. CRUD Table Pelanggan
    4. CRUD Table Pemasok
3. **Example Test**
    1. Example Test Table Barang
    2. Example Test Table Transaksi
    3. Example Test Table Pelanggan
    4. Example Test Table Pemasok

## 1. Instalation and Configuration

### Install the Prerequisites of The Project
---
To use this project we need to clone it first in :

`git clone git@gitlab.com:glints-11-tim-a/orm-sequelize.git`

Then install dependencies with npm :

`npm install --save express multer mysql2 validator express-validator dotenv sequelize`

Then init the project :

`npm init`

just press 'enter' until it finish. in the dependencies section in package.json it should look like this :
```json
{
  "dependencies": {
    "dotenv": "^8.2.0",
    "express": "^4.17.1",
    "express-validator": "^6.10.0",
    "multer": "^1.4.2",
    "mysql2": "^2.2.5",
    "sequelize": "^6.6.2"
  },
}
```
after this we need to configure the sequelize database. Open file config.json in config folder, then change the development database configuration (just left the production and test section like that, we didn't use it) :

```json
{
  "development": {
    "username": "CHANGE_TO_YOUR_DB_USERNAME",
    "password": "CHANGE_TO_YOUR_DB_PASSWORD",
    "database": "CHANGE_TO_YOUR_DB_NAME",
    "host": "CHANGE_TO_YOUR_HOST_LOCATION(e.g 127.0.0.1)",
    "dialect": "mysql"
  },
}
```
To start config the Databases we need to install `sequelize-cli`, to do that, just run this code :

`npm install --save -g sequelize-cli`

After that, go to your project folder in your terminal and type `sequelize` then `press "enter"` to check the instalation success or not. The result should be like this

```
Sequelize CLI [Node: 14.16.0, CLI: 6.2.0, ORM: 6.6.2]

sequelize <command>

Commands:
sequelize db:migrate  .........something else here..........
```
### Create Database and Tables
---

After we install the prerequisites, now we will create the Database to make the application running well. first run this code to create database according to our configuration that we made in the last section.

`sequelize db:create`  

then run this code to create the tables :

`sequelize db:migrate`

then run this code to populate the tables with dummy data :

`sequelize db:seed:all`

now we were ready to run this app.

### Running The App
---
we can use the basic node command to run it `node index` or we can install `nodemon` first, then use this command to run the project `npm run dev`.

**_Don't forget to run the Database Provider such as MySQL before running the App_**

## **2. Basic Usage**
Here some guide to use this project. but first don't forget to run the App/server in the terminal (see _"Running The App"_ section).

to make us easy to test or use this App, we can install Postman application.

### 2.1 CRUD Table Barang
---

#### **Get All Data**
To get all data we need to access this address with GET Method :

`http:localhost:3000/barang`

#### **Get One Data**
To get all data we need to access this address with GET Method :

`http:localhost:3000/barang/THE_ID`

replace _THE_ID_ with our barang id that we want to read/get.
#### **Insert Data**
To insert data to barang table we need to access this address with POST Method :

`http:localhost:3000/barang/create`

the form-body data should use this format :
```
nama:
harga:
id_pemasok:
image:
```
#### **Update Data**
To update data to barang table we need to access this address with PUT Method :

`http:localhost:3000/barang/update/THE_ID`

and replace _THE_ID_ with our barang id that we want to update.

then use form-body data with this format :
```
nama:
harga:
id_pemasok:
image:
```

#### **Delete Data**
To delete data to barang table we need to access this address with DELETE Method :

`http:localhost:3000/barang/THE_ID`

and replace _THE_ID_ with our barang id that we want to delete.

### 2.2 CRUD Table Transaksi
---

#### **Get All Data**
To get all data we need to access this address with GET Method :

`http:localhost:3000/transaksi`

#### **Get One Data**
To get all data we need to access this address with GET Method :

`http:localhost:3000/transaksi/THE_ID`

replace _THE_ID_ with our transaksi id that we want to read/get.
#### **Insert Data**
To insert data to transaksi table we need to access this address with POST Method :

`http:localhost:3000/transaksi/insert`

the form-body data should use this format :
```
id_barang:
id_pelanggan:
jumlah:
fakturImg:
```
#### **Update Data**
To update data to transaksi table we need to access this address with PUT Method :

`http:localhost:3000/transaksi/update/THE_ID`

and replace _THE_ID_ with our transaksi id that we want to update. then use form-body data with this format :
```
id_barang:
id_pelanggan:
jumlah:
fakturImg:
```

#### **Delete Data**
To delete data to transaksi table we need to access this address with DELETE Method :

`http:localhost:3000/transaksi/delete/THE_ID`

and replace _THE_ID_ with our transaksi id that we want to delete.

### 2.3 CRUD Table Pelanggan
---

#### **Get All Data**
To get all data we need to access this address with GET Method :

`http:localhost:3000/pelanggan`

#### **Get One Data**
To get all data we need to access this address with GET Method :

`http:localhost:3000/pelanggan/THE_ID`

replace _THE_ID_ with our pelanggan id that we want to read/get.
#### **Insert Data**
To insert data to pelanggan table we need to access this address with POST Method :

`http:localhost:3000/pelanggan/create`

the form-body data should use this format :
```
nama:
alamat:
noTelp:
image:
```
#### **Update Data**
To update data to pelanggan table we need to access this address with PUT Method :

`http:localhost:3000/pelanggan/update/THE_ID`

and replace _THE_ID_ with our pelanggan id that we want to update.

then use form-body data with this format :
```
nama:
alamat:
noTelp:
image:
```

#### **Delete Data**
To delete data to pelanggan table we need to access this address with DELETE Method :

`http:localhost:3000/pelanggan/THE_ID`

and replace _THE_ID_ with our pelanggan id that we want to delete.

### 2.4 CRUD Table Pemasok
---

#### **Get All Data**
To get all data we need to access this address with GET Method :

`http:localhost:3000/pemasok`

#### **Get One Data**
To get all data we need to access this address with GET Method :

`http:localhost:3000/pemasok/THE_ID`

replace _THE_ID_ with our pemasok id that we want to read/get.
#### **Insert Data**
To insert data to pemasok table we need to access this address with POST Method :

`http:localhost:3000/pemasok/create`

the form-body data should use this format :
```
nama:
alamat:
email:
no_hp:
image:
```
#### **Update Data**
To update data to pemasok table we need to access this address with PUT Method :

`http:localhost:3000/pemasok/THE_ID`

and replace _THE_ID_ with our pemasok id that we want to update.

then use form-body data with this format :
```
nama:
alamat:
email:
no_hp:
image:
```

#### **Delete Data**
To delete data to pemasok table we need to access this address with DELETE Method :

`http:localhost:3000/pemasok/THE_ID`

and replace _THE_ID_ with our pemasok id that we want to delete.

## **3. Example Test**
Here some test that we made
### 3.1 Example Test CRUD Barang
---

![barang_min.gif](./barang_min.gif)

### 3.2 Example Test CRUD Transaksi
---

![transaksi_min.gif](./transaksi_min.gif)

### 3.3 Example Test CRUD Pelanggan
---

![pelanggan_min.gif](./pelanggan_min.gif)

### 3.4 Example Test CRUD Pemasok
---

![pemasok_min.gif](./pemasok_min.gif)
