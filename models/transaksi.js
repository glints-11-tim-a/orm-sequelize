"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class transaksi extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  transaksi.init(
    {
      id_barang: DataTypes.INTEGER,
      id_pelanggan: DataTypes.INTEGER,
      jumlah: DataTypes.INTEGER,
      total: DataTypes.DECIMAL,
      fakturImg: {
        type : DataTypes.STRING,
        get() {
          const image = this.getDataValue("fakturImg");
          return "/images/" + image;
        },
      },
      createdAt:{
        type: DataTypes.DATE,
        get() {
          const dt = new Date(this.getDataValue("createdAt"));
          return new Date(dt-dt.getTimezoneOffset() * 60000);
        },
      },
      updatedAt:{
        type: DataTypes.DATE,
        get() {
          const dt = new Date(this.getDataValue("updatedAt"));
          return new Date(dt-dt.getTimezoneOffset() * 60000);
        },
      },
    },
    {
      sequelize,
      modelName: "transaksi",
      paranoid: true,
      timestamps: true,
      freezeTableName: true,
    }
  );
  return transaksi;
};
